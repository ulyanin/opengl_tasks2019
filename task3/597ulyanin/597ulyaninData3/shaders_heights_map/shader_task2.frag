/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

struct TextureMaterial
{
    float Ka;
    float Kd;
    float Ks;
    float Height;
    sampler2D DiffuseTex;
};


uniform TextureMaterial water;
uniform TextureMaterial grass;
uniform TextureMaterial rock;
uniform TextureMaterial snow;
uniform float WaterLevel;


in vec2 texCoord; // текстурные координаты (интерполирована между вершинами треугольника)
in vec3 lightColor; // Освещенность (интерполирована между вершинами треугольника)
in float height; // текущая высота (интерполирована между вершинами треугольника)

out vec4 fragColor; //  цвет фрагмента


const float sigma = 0.7;

float norm_coef(float x, float mu, float sigma_) {
    return exp(-((x - mu) * (x - mu) / (2 * sigma_*sigma_)));
}


void main()
{

    vec3 diffuseColor =
        + texture(grass.DiffuseTex, texCoord).rgb * norm_coef(height, 2*sigma/7, sigma/7)
        + texture(rock.DiffuseTex, texCoord).rgb * norm_coef(height, 4*sigma/7, sigma/7)
        + texture(snow.DiffuseTex, texCoord).rgb * norm_coef(height, 0.9, 3 * sigma/7);
    if (height < WaterLevel) {
        diffuseColor = texture(water.DiffuseTex, texCoord).rgb * norm_coef(height, 0, WaterLevel / 2);
    }


    fragColor = vec4(diffuseColor * lightColor, 1.0);
}
