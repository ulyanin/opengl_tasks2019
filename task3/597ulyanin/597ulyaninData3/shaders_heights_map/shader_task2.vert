/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 330

struct LightInfo
{
    vec3 dir; // положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; // цвет и интенсивность окружающего света
    vec3 Ld; // цвет и интенсивность диффузного света
    vec3 Ls; // цвет и интенсивность бликового света
};


uniform LightInfo light;

// стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

// матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;


layout(location = 0) in vec3 vertexPosition; // координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; // нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; // текстурные координаты вершины


//out vec3 normalCamSpace; // нормаль в системе координат камеры
//out vec4 posCamSpace; // координаты вершины в системе координат камеры
out vec2 texCoord; // текстурные координаты
out vec3 lightColor; // Освещенность
out float height; // текущая высота

void main()
{
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

    // Out1
    texCoord = vertexTexCoord;

    // For directional light
    vec3 normalCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
    vec4 lightDirCamSpace = viewMatrix * normalize(vec4(light.dir, 0.0));
    float NdotL = max(dot(normalCamSpace, lightDirCamSpace.xyz), 0.0);

    // Out2
    lightColor = (light.La + light.Ld * NdotL);

    // Out3
    height = vertexPosition[2];
}
